{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Notebook 3 – Continuum mechanics\n",
    "\n",
    "### What is continuum mechanics? What is the continuum mechanics procedure?\n",
    "\n",
    "#### _Purpose of today's exercise_\n",
    "\n",
    "The goals of today's exercise are to:\n",
    "\n",
    "* understand continuum mechanics concepts and its procedure\n",
    "\n",
    "* understand the traffic problem and rocket problem in continuum mechanics context\n",
    "\n",
    "* practice nondimensionalization\n",
    "\n",
    "#### _Contents_\n",
    "\n",
    "[**Continuum mechanics: Formulating a problem**](#continuum.mechanics)\n",
    "\n",
    "[**Continuum mechanics procedure by example: Traffic problem and rocket problem**](#traffic.rocket)\n",
    "\n",
    "[**Nondimensionalization practice**](#nondim)\n",
    "\n",
    "#### _Submission format_\n",
    "\n",
    "Please submit the result of your exercises by \n",
    "\n",
    "> * Download the notebook to your computer **as a PDF (.pdf) file**.\n",
    "> \n",
    ">   **File > Export notebook as... > Export notebook to PDF**\n",
    "> \n",
    "> * Upload the notebook to the Jupyter Notebook 3 assignment in Moodle.\n",
    "\n",
    "Don't forget to complete the interactive questions (3) at the end before submitting.\n",
    "\n",
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='continuum.mechanics'><a>\n",
    "### Continuum mechanics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first lecture of the course covered important concepts about continuum mechanics, which were continued this week. You are invited to reflect on these concepts in the following questions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 1** \n",
    "\n",
    "Can you list 3 topics where continuum mechanics is applied (for example, \"modeling avalanches and snow\" or \"studying the atmosphere\")? Several examples were given in lecture, or feel free to use your own."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. your answer here\n",
    "2. your answer here\n",
    "3. your answer here\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 2** \n",
    "\n",
    "**a)** In your own words, **what is the continuum hypothesis?** \n",
    "\n",
    "**b)** For one of the topics you listed in **Exercise 1** (whether the material is solid/liquid/gas or even a granular material such as snow or sand), please explain the continuum hypothesis in terms of the density inside an imagined volume of material, which is an imaginary box of volume $l^3$ and side length $l$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**a)** ... your answer here ...\n",
    "\n",
    "**b)** ... your answer here ...\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 3** \n",
    "\n",
    "The continuum hypothesis applies **between certain scales.** For an example topic you chose from **Exercise 1**, at what **small scale** does it become unrealistic to assume the continuum hypothesis for that material? \n",
    "\n",
    "_Note:_ There is an **upper scale** where it's unrealistic to assume the continuum hypothesis too, usually greater than the planetary scale."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... your answer here ...\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 4:** Rocket problem.\n",
    "\n",
    "In the following example, we will see a real case where the scales involved in a problem (in this case, the scale of the velocity of a rocket compared to a planet's radius and gravitational pull) determine whether an approximate theory applies. This type of analysis often happens in continuum mechanics. In this case, the approximate theory is the projectile motion equation $x(t) = -\\frac{1}{2} g t^2 + v_0 t$. \n",
    "\n",
    "**There are 2 solutions that give the rocket's trajectory given an initial launch velocity $v_0$:**\n",
    "\n",
    "**1. approximate solution, projectile motion, $x(t) = -\\frac{1}{2} g t^2 + v_0 t$**\n",
    "* Applies when gravitational pull does not change much over the flight\n",
    "    \n",
    "**2. full solution, which we must find numerically or experimentally**\n",
    "* Applies when gravitational pull changes significantly over the flight\n",
    "\n",
    "**The key to the problem is to find a dimensionless parameter $\\varepsilon$ that tells us when to transition from one theory to another. When is $\\varepsilon$ small enough to approximate the solution using method (1) and when must we use method (2)?**\n",
    "\n",
    "In class we took an equation of motion for an object under gravity\n",
    "\n",
    "\\begin{equation}\n",
    "    m \\frac{d^2 x}{dt^2} = -\\frac{mgR^2}{(R + x)^2}\n",
    "\\end{equation}\n",
    "\n",
    "with initial conditions\n",
    "\n",
    "\\begin{align}\n",
    "     x(0) &= 0 \\quad \\text{(intial position)}, \\\\\n",
    "     \\dot{x}(0) &= v_0 \\quad \\text{(initial velocity)}\n",
    "\\end{align}\n",
    "\n",
    "where\n",
    "\n",
    "* $x$ = rocket's position (always $>0$)\n",
    "* $t$ = time\n",
    "* $\\frac{d^2x}{dt^2}$ = rocket's acceleration\n",
    "* $R$ = planet's radius\n",
    "* $g$ = gravitational constant\n",
    "* $v_0$ = rocket's initial velocity (assuming it is powered only at the beginning)\n",
    "\n",
    "and nondimensionalized it to get\n",
    "\n",
    "\\begin{equation}\n",
    "    \\frac{d^2y}{d\\tau^2} = -\\frac{1}{(1 + \\varepsilon y)^2}\n",
    "\\end{equation}\n",
    "\n",
    "with initial conditions\n",
    "\n",
    "\\begin{align}\n",
    "     y(0) &= 0 \\quad \\text{(intial position)}, \\\\\n",
    "     \\dot{y}(0) &= 1 \\quad \\text{(initial velocity)}\n",
    "\\end{align}\n",
    "\n",
    "where\n",
    "\n",
    "* $y$ = rocket's nondimensional position, $y = \\frac{x}{x_{sc}} = \\frac{x}{v_0^2/g}$\n",
    "* $\\tau$ = nondimensional time, $\\tau = \\frac{t}{t_{sc}} = \\frac{t}{v_0/g}$\n",
    "* $\\frac{d^2y}{d\\tau^2}$ = rocket's nondimensional acceleration\n",
    "* $\\varepsilon$ is a nondimensional constant combining all the other constant terms, $\\varepsilon = v_0^2/(gR)$\n",
    "---\n",
    "\n",
    "**Numerical solution for the height of the rocket as a function of time.**\n",
    "\n",
    "Below is a numerical solution for the nondimensional equation. Run each of the 4 code cells below.\n",
    "\n",
    "**! - You will only need to edit the 3rd one** – the numerical solution is a nice demonstration, but you will not be expected to produce anything like this (beyond the scope of a continuum mechanics class). For our purposes, focus on understanding the effect of nondimensional parameter $\\varepsilon$ on the solution. After any change you choose to make in the 3rd code cell, be sure to re-run all the cells to see how the plot has changed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 49,
   "metadata": {},
   "outputs": [],
   "source": [
    "################# IMPORTS #################\n",
    "# This section imports a few packages and\n",
    "# makes sure the plot shows up inline.\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from scipy.integrate import odeint\n",
    "%matplotlib widget"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 50,
   "metadata": {},
   "outputs": [],
   "source": [
    "########## DIFFERENTIAL EQUATION ##########\n",
    "# This section defines a function that is the differential equation for the system.\n",
    "def D(z, tau, epsilon):\n",
    "    y = z[0]\n",
    "    q = z[1]\n",
    "    return np.array([q, -1/(1 + epsilon*y)**2])  # Notice here we have used the nondimensional equation -1/(1 + epsilon y)^2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "**Modify the code in the cell below.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 51,
   "metadata": {},
   "outputs": [],
   "source": [
    "########## SELECTING AN EPSILON ########### <- Modify the code in this section.\n",
    "epsilon_list = [0, 0.01, 0.1, 1, 3]  # value(s) of epsilon that appear in the plot\n",
    "\n",
    "######### DEFINING A HEIGHT AXIS ##########\n",
    "height_max = 2  # max plot height           <- You may choose to modify this.\n",
    "\n",
    "######### DEFINING A TIME AXIS ############\n",
    "tau_max = 6  # max nondimensional time     <- You may choose to modify this.\n",
    "tau_step = 0.01  # time step between each calculation\n",
    "tau = np.linspace(0, tau_max, num=int(np.round(tau_max/tau_step))+1)  # make a list of points between 0 and tau_max"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 52,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "a236248cb9694dbb9f2cb839d214c041",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Canvas(toolbar=Toolbar(toolitems=[('Home', 'Reset original view', 'home', 'home'), ('Back', 'Back to previous …"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/usr/local/lib/python3.6/dist-packages/scipy/integrate/odepack.py:247: ODEintWarning: Excess work done on this call (perhaps wrong Dfun type). Run with full_output = 1 to get quantitative information.\n",
      "  warnings.warn(warning_msg, ODEintWarning)\n",
      "/usr/local/lib/python3.6/dist-packages/scipy/integrate/odepack.py:247: ODEintWarning: Excess work done on this call (perhaps wrong Dfun type). Run with full_output = 1 to get quantitative information.\n",
      "  warnings.warn(warning_msg, ODEintWarning)\n"
     ]
    }
   ],
   "source": [
    "# Initialize a plot\n",
    "plt.figure(figsize=(8, 6))\n",
    "\n",
    "################ SOLVING ###################\n",
    "for epsilon in epsilon_list:\n",
    "    # Solves the differential equation using `odeint` package\n",
    "    y_sol = odeint(D, [0, 1], tau, args=(epsilon,))[:, 0]\n",
    "    # The next 3 lines remove values where the rocket goes below the surface of the planet\n",
    "    landing = next((i for i, position in enumerate(y_sol) if position < 0), None)\n",
    "    y_sol_until_landing = y_sol[:landing]\n",
    "    tau_until_landing = tau[:landing]\n",
    "    # Plots the solution \n",
    "    plt.plot(tau_until_landing, y_sol_until_landing, label='$\\epsilon$ = ' + str(epsilon))\n",
    "\n",
    "################################################# CODE FOR PLOTTING ###############################################\n",
    "# Plot the approximate equation y(tau) = -1/2(tau^2) + tau, the nondimensional form of -1/2(g)(t^2) + v_0 * t\n",
    "plt.plot(tau, -1/2*tau**2 + tau, label='approx', linestyle=':', color='black')\n",
    "\n",
    "# Set limits on the x-axis and y-axis\n",
    "plt.xlim([0, tau_max])\n",
    "plt.ylim([0, height_max])\n",
    "\n",
    "# Label the axes\n",
    "plt.xlabel('$t$ (nondimensional time)')\n",
    "plt.ylabel('$y$ (nondimensional height)')\n",
    "plt.title('Rocket launches for different $\\\\varepsilon$')\n",
    "\n",
    "# Plot a grid in the background\n",
    "plt.grid()\n",
    "\n",
    "# Add a legend in the upper left corner, with a border\n",
    "plt.legend(loc='upper left', frameon=True)\n",
    "\n",
    "# Show the plot in the Jupyter notebook\n",
    "plt.show()\n",
    "####################################################################################################################"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**a)** Run each of the 4 code cells to view the plot after the last one. The approximate solution (1) for projectile motion is plotted in a **black dotted line**. The full numerical solution (2) for a rocket with different values of $\\varepsilon$ is plotted in lines of **various colors**. Describe the rocket's behavior at different $\\varepsilon$.\n",
    "\n",
    "**b)** For which values of $\\varepsilon$ does the rocket trajectory appear reasonably close to the approximate solution (**black dotted line**)? To answer our question above – in your opinion, when is $\\varepsilon$ small enough to approximate the solution using method (1) and when must we use method (2)?\n",
    "\n",
    "**c)** In the 3rd code cell, labeled \"**Modify the code in the cell below**,\" change the list of $\\varepsilon$ values to see how closely or not the rocket follows the approximate theory (**black dotted line**) at large and small $\\varepsilon$. By experimenting with different $\\varepsilon$, can you identify at which $\\varepsilon$ the rocket goes into orbit rather than coming back to earth? This definitely is outside the range of projectile motion!\n",
    "\n",
    "_Hint:_ You may also choose to modify the plot height limit `height_max` or time limit `tau_max` if your trajectories go out of range. \n",
    "\n",
    "_Hint hint:_ Try not to make `tau_max` extremely large, though, or the calculation may take too long. \n",
    "\n",
    "**d)** Why nondimensionalize? We have the following constants $g$ and $R$ for Earth, Mars, and Jupiter. \n",
    "\n",
    "Planet  | Gravitational constant $g$ (m/s$^2$) | Radius $R$ (km)\n",
    "--------|:------------------------------------:|:---------------:\n",
    "Earth   |                 9.8                  |       6,400\n",
    "Mars    |                 3.7                  |       3,400 \n",
    "Jupiter |                24.8                  |      71,400\n",
    "\n",
    "If we didn't nondimensionalize the equation above, we would need to repeat this numerical calculation 3 times to get the velocity required to go into orbit for each of these planets. This is called the \"escape velocity.\" Instead, we can use nondimensional parameter $\\varepsilon$ for all 3 cases! Can you use the $\\varepsilon$ value you estimate from **c)** to calculate the escape velocity required to go into orbit on these 3 planets?\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**a)** ... your answer here ...\n",
    "\n",
    "**b)** ... your answer here ...\n",
    "\n",
    "**c)** ... your answer here ...\n",
    "\n",
    "**d)** Put your answers in the table.\n",
    "\n",
    "Planet  | Escape velocity $v_0$ (m/s) |\n",
    "--------|-----------------------------|\n",
    "Earth   | ?\n",
    "Mars    | ?\n",
    "Jupiter | ?      \n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='traffic.rocket'><a>\n",
    "### Continuum mechanics procedure by example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 5:** The continuum mechanics procedure.\n",
    "\n",
    "Considering science in general, we may say that science is both\n",
    "\n",
    "**1. a collection of topics** (for example, biology, chemistry, physics, ...) **AND**\n",
    "\n",
    "**2. a procedure** (the scientific method).\n",
    "\n",
    "The procedure part, the scientific method, has a series of familiar steps:\n",
    "\n",
    "* ask a question, \n",
    "* form a hypothesis, \n",
    "* design an experiment, \n",
    "* collect data,\n",
    "* compare results to the hypothesis\n",
    "* draw a conclusion.\n",
    "\n",
    "As you are beginning to see from the class, continuum mechanics is similarly both \n",
    "\n",
    "**1. a collection of topics** (fluid dynamics, solid mechanics, heat transfer, ... see also the examples you gave in **Exercise 1**) **AND**\n",
    "\n",
    "**2. a procedure** (we could call it \"the continuum mechanics method\").\n",
    "\n",
    "The following are the steps of the continuum mechanics method:\n",
    "\n",
    "No. | Step                                                          | Note\n",
    "----|---------------------------------------------------------------|-----------\n",
    "1   |**Describe the problem**                                       | \n",
    "2   |**Identify independent and dependent variables**               | including their units and dimensions\n",
    "3   |**Draw free-body diagram**                                     | including all variables labeled\n",
    "4   |**Write basic mechanics law(s) in the form of an equation**    | some equation(s) that applies to the problem (for example F = ma)\n",
    "5   |**Define boundary and/or initial conditions**                  | or other constraints\n",
    "6   |**Nondimensionalize equation**                                 |\n",
    "7   |**Simplify equation by analyzing nondimensional paramters**    | for example, a small $\\varepsilon$ may let us get rid of terms\n",
    "8   |**Get a governing equation**                                   | \n",
    "•   |(**Solve the governing equation**)                             | not part of our job in this class, now it's a problem for math, numerics, or experiment\n",
    "\n",
    "Below are the steps used in both the **traffic problem** example and the **rocket problem** example. Continuum mechanics equations are usually more complex than both these problems, so take these simplified problems as an opportunity to understand the procedure.\n",
    "\n",
    "---\n",
    "\n",
    "**! - For the problems below, you only need to fill in a number to replace the '?' where indicated. No need to resolve the problem.**\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**a)** The steps of the **traffic problem** are written out of order here, can you identify each step in the continuum mechanics procedure? **Fill in the correct number at the '?'s.** \n",
    "\n",
    "(Not all the procedure steps are used in this one.)\n",
    "\n",
    "* `Step ?` – Draw a line representing the highway and position $x$ along it, where we analyze a certain section beginning at position $a(t)$ and ending at position $b(t)$.\n",
    "\n",
    "* `Step ?` – Arrive at the equation for car density\n",
    "$$\n",
    "\\frac{\\partial \\rho}{\\partial t} + \\frac{\\partial (\\rho u)}{\\partial x} = \\frac{\\partial \\rho}{\\partial t} + \\frac{\\partial}{\\partial x} \\left[\\rho \\cdot 0.5 \\ln\\left(\\frac{0.142}{\\rho}\\right) \\right] = 0\n",
    "$$\n",
    "\n",
    "* `Step ?` – Write the experimentally determined constraint\n",
    "$$\n",
    "u(\\rho) = 0.5 \\ln\\left(\\frac{0.142}{\\rho}\\right).\n",
    "$$\n",
    "\n",
    "* `Step ?` – How does the velocity of cars in traffic on a highway depend on how packed together the cars are?\n",
    "\n",
    "* `Step ?` – Write that the number of cars on a highway is equal to the integral of the density\n",
    "$$\n",
    "N = \\int_{a(t)}^{b(t)} \\rho(x, t) dx\n",
    "$$\n",
    "and that cars are conserved\n",
    "$$\n",
    "\\frac{dN}{dt} = 0.\n",
    "$$\n",
    "\n",
    "* `Step ?` – Define $t$ as time, $x$ as position along the highway, and the functions $u(x, t)$ for car flow velocity and $\\rho(x, t)$ for car density."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**b)** The steps of the **rocket problem** are written out of order here, can you identify each step in the continuum mechanics procedure? **Fill in the correct number at the '?'s.** \n",
    "\n",
    "* `Step ?` – The initial velocity of the rocket is $x'(0) = v_0$. Its initial position is $x(0) = 0$.\n",
    "\n",
    "* `Step ?` – Using the constants in the equation, get a characteristic time scale $t_{sc} = v_0 / g$ and a characteristic length scale $x_{sc} = v_0^2 / g$. Divide the time $t$ and length $x$ by these values to get new scales $\\tau = t / t_{sc}$ and $y = x / x_{sc}$ in the equation of motion, resulting in\n",
    "$$\\frac{d^2y}{d\\tau^2} = -\\frac{1}{(1 + \\varepsilon y)^2}$$\n",
    "\n",
    "* `Step ?` – Define $t$ as time, $x$ as position, and constants $g$, $R$, and $v_0$.\n",
    "\n",
    "* `Step ?` – Integrate to get $y(\\tau) = -\\frac{1}{2} \\tau^2 + \\tau$ (in nondimensional form).\n",
    "\n",
    "* `Step ?` – Write the equation $F = m \\frac{d^2 x}{dt^2} = \\frac{G m M}{r^2}$.\n",
    "\n",
    "* `Step ?` – What is the height of a rocket launched with some initial velocity under gravity?\n",
    "\n",
    "* `Step ?` – Simplify the equation of motion to get a nondimensional parameter $\\varepsilon = \\frac{v_0^2}{gR}$. Assuming epsilon is small, get rid of the term with $\\epsilon$ in the denominator, resulting in \n",
    "$$\\frac{d^2y}{d\\tau^2} = -1$$\n",
    "\n",
    "* `Step ?` – Draw a diagram of a planet labeled with its radius $R$, rocket position $x$, rocket initial velocity $x'(0) = v_0$, and rocket initial position $x(0) = 0$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='nondim'><a>\n",
    "### Nondimensionalization"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 6:** Rescaling practice.\n",
    "\n",
    "Transform the term \n",
    "$$u \\frac{\\partial u}{\\partial t}$$ \n",
    "using new variables $w = u/u_{sc}$, $\\tau = t/t_{sc}$,\n",
    "where $u_{sc}$ and $t_{sc}$ are the constant scales.\n",
    "\n",
    "_Hint:_ The result should be a dimensional coefficient multiplied by a nondimensional term.\n",
    "\n",
    "Please type your answer in the box below using LaTeX (try to, if it doesn't work due to Noto, just do the exercise by hand for your own practice and leave this blank). See Notebook 1 or the guide on Moodle if stuck with LaTeX.\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... your answer here ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Interactive questions\n",
    "\n",
    "##### _Answer before submitting notebook to Moodle_ \n",
    "\n",
    "Run the cell below. If you don't see the questions, please make sure you are logged into both Moodle and Noto in the same browser. (May not work with private browsing.)\n",
    "\n",
    "<iframe src=\"https://moodle.epfl.ch/mod/hvp/embed.php?id=1129198\" width=\"1361\" height=\"208\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\"></iframe><script src=\"https://moodle.epfl.ch/mod/hvp/library/js/h5p-resizer.js\" charset=\"UTF-8\"></script>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "\n",
       "        <iframe\n",
       "            width=\"800\"\n",
       "            height=\"500\"\n",
       "            src=\"https://moodle.epfl.ch/mod/hvp/embed.php?id=1135085\"\n",
       "            frameborder=\"0\"\n",
       "            allowfullscreen\n",
       "        ></iframe>\n",
       "        "
      ],
      "text/plain": [
       "<IPython.lib.display.IFrame at 0x7fe8b51c0860>"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from IPython.display import IFrame\n",
    "IFrame('https://moodle.epfl.ch/mod/hvp/embed.php?id=1135085', 800, 500)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### End of notebook"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
